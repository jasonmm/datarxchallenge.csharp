using System.Collections.Generic;

namespace DataRxChallenge
{
    /// <summary>
    /// Contains methods to find a given word in a given word search grid.
    /// </summary>
    public class FindWord
    {
        /// <summary>
        /// The directions of the compass.
        /// </summary>
        public static readonly int[,] Directions = {
            {1, 0},
            {1, 1},
            {0, 1},
            {-1, 1},
            {-1, 0},
            {-1, -1},
            {0, -1},
            {1, -1},
        };

        private char[,] Grid;
        private string WordToFind;
        private int XDimension;
        private int YDimension;
        
        public List<Occurrence> Occurrences = new List<Occurrence>();


        /// <summary>
        /// Finds the given word in the given word search grid.
        /// </summary>
        public int Find(char[,] grid, string word)
        {
            Grid = grid;
            XDimension = Grid.GetLength(1);
            YDimension = Grid.GetLength(0);
            WordToFind = word;
            Occurrences.Clear();

            for (var x = 0; x < XDimension; x++)
            {
                for (var y = 0; y < YDimension; y++)
                {
                    if (Grid[y, x] == WordToFind[0])
                    {
                        SearchCompassDirections(x, y);
                    }
                }
            }
            return Occurrences.Count;
        }

        /// <summary>
        /// Looks to see if the word being searched for exists starting at
        /// the given x,y coordinate.
        /// </summary>
        public void SearchCompassDirections(int x, int y)
        {
            for (var i = 0; i < 8; i++)
            {
                var str = GetLettersInDirection(x, y, i);
                if (str == WordToFind)
                {
                    Occurrences.Add(new Occurrence
                    {
                        x = x,
                        y = y,
                        directionIndex = i
                    });
                }
            }
        }

        /// <summary>
        /// Returns the string comprised of the letters found by starting at
        /// the given x,y and moving in the given direction.
        /// </summary>
        public string GetLettersInDirection(int x, int y, int directionIndex)
        {
            var str = "";
            var dx = Directions[directionIndex, 0];
            var dy = Directions[directionIndex, 1];

            // Check to see if the word can even fit at this spot.
            var endX = x + (WordToFind.Length * dx);
            if (endX < 0 || endX > XDimension)
            {
                return "";
            }
            var endY = y + (WordToFind.Length * dy);
            if (endY < 0 || endY > YDimension)
            {
                return "";
            }

            for (var i = 0; i < WordToFind.Length; i++)
            {
                str += Grid[y, x];
                x += dx;
                y += dy;
            }

            return str;
        }
    }
}