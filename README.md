# DataRx Challenge in C# #

The program reads files starting with the string "grid" from the _grids_ directory and searches them for a given word.  The word to search for is specified as a command-line argument.

## Usage

Tested using the following .NET Core version:

    $ dotnet --version
    1.0.0-preview1-002702

To clone, build, and run:

    $ git clone https://jasonmm@bitbucket.org/jasonmm/datarxchallenge.csharp.git
    $ dotnet restore
    $ dotnet build
    $ dotnet run bread
   
Output:

    Project datarxchallenge.csharp (.NETCoreApp,Version=v1.0) was previously compiled. Skipping compilation.
    bread found 4 times in grids/grid-jagged.txt.
    bread found 4 times in grids/grid-large.txt.
    bread found 4 times in grids/grid.txt.