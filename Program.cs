﻿using System;
using System.IO;

namespace DataRxChallenge
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Missing argument.  Please specify the word to search for as the first argument.");
                return;
            }

            var wordFinder = new FindWord();

            var gridFilenames = Directory.EnumerateFiles("grids/", "grid*");
            foreach (var fileName in gridFilenames)
            {
                var grid = Program.ReadGridFromFile(fileName);
                var occurrenceCount = wordFinder.Find(grid, args[0]);
                Console.WriteLine($"{args[0]} found {occurrenceCount} times in {fileName}.");
            }
        }

        /// <summary>
        /// Converts the data in the file into a two-dimensional array of 
        /// characters.
        /// </summary>
        public static char[,] ReadGridFromFile(string fileName)
        {
            var allLines = File.ReadAllLines(fileName);

            // All lines must have the same length; we ensure this later.
            var lineLength = allLines[0].Length;
            var grid = new char[allLines.Length, allLines[0].Length];

            for (var j = 0; j < allLines.Length; j++)
            {
                // The ToCharArray() has parameters so we don't accidentally 
                // get a longer string than our grid array can hold; just in 
                // case the input is not formatted correctly.
                var lineChars = allLines[j].ToCharArray(0, grid.GetLength(1));
                for (var i = 0; i < lineChars.Length; i++)
                {
                    grid[j, i] = lineChars[i];
                }
            }

            return grid;
        }
    }
}
