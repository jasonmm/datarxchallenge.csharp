namespace DataRxChallenge
{
    /// <summary>
    /// Holds information about a specific occurrence of the word in the word 
    /// search grid.
    /// </summary>
    public class Occurrence
    {
        public int x;
        public int y;
        public int directionIndex;
    }
}